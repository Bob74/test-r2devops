FROM python:3.10-slim-bullseye as builder
ENV PYROOT=/venv
ENV PYTHONUSERBASE=$PYROOT
WORKDIR /
COPY pyproject.toml ./
RUN apt-get update &&\
  apt-get install --no-install-recommends -y build-essential libffi-dev libssh-dev python3-dev &&\
  pip install --no-cache-dir poetry==1.2.0 setuptools==65.3.0 pip==22.2.2 --upgrade &&\
  poetry lock &&\
  poetry export -f requirements.txt --output requirements.txt &&\
  pip3 wheel -r requirements.txt

FROM python:3.10-slim
WORKDIR /
COPY --from=builder /*.whl /wheels/
RUN apt-get update &&\
  apt-get install --no-install-recommends -y gettext &&\
  pip3 install --no-cache-dir /wheels/*.whl &&\
  rm -rf /wheels &&\
  apt-get autoclean &&\
  apt-get clean
